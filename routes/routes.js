const {Router} = require('express');
const controllers = require('../controllers/controllers');
const router = new Router();


router.post('/create-usuario',controllers.createUsuario);
router.get('/get-usuario/:_id',controllers.getUsuario);
router.get('/get-usuarios',controllers.getUsuarios);
router.put('/update-usuario/:_id',controllers.updateUsuario);
router.delete('/delete-usuario/:_id',controllers.deleteUsuario);
router.post('/login',controllers.login);

module.exports = router;