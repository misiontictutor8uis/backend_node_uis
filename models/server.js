const express = require('express');
const cors = require('cors');
const conectarBD = require('../database/config');
const path = require('path');
require('dotenv').config()


class Server {
  app = express();
  puerto = process.env.PORT;
  
  constructor(){
    this.middlewares();
    this.rutas();
    conectarBD();
    this.listen();
  }

  middlewares(){
    this.app.use( express.json() );
    this.app.use( cors() );
    this.app.use( express.static('public') ); //CARPETA PUBLICA VA ANGULAR COMPILADO
  }

  rutas(){
    this.app.use('/api', require('../routes/routes'));
    this.app.get('*', (req,res) => { res.sendFile( path.resolve('../public/index.html') ) })
  }

  listen(){
    this.app.listen(this.puerto, ()=>{console.log("Corriendo en puerto " + this.puerto)})
  }

}

module.exports = Server;