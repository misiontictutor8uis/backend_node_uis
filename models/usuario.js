const {Schema, model} = require('mongoose');

const usuarioSchema = new Schema({
  nombres: String,
  email: String,
  password: String,
  estado: Boolean,
});

const Usuario = model("usuario", usuarioSchema);

module.exports = Usuario;