const {
  request,
  response
} = require('express');
const bcryptjs = require('bcryptjs');
const Usuario = require('../models/usuario');


const login = async (req = request, res = response) => {

  const {
    nombre,
    password,
    estado
  } = req.body;

  const usuarioBuscado = await Usuario.findOne({
    nombre
  });
  if (!usuarioBuscado) {
    return res.status(400).json({
      mensaje: "Usuario no existe",
      login: false
    });
  }

  if (!bcryptjs.compareSync(password, usuarioBuscado.password)) {
    return res.status(400).json({
      mensaje: "Contraseña invalida",
      login: false
    });
  }

  res.json({
    login: true
  })

}

//CRUD USUARIOS
async function createUsuario(req = request, res = response) {

  // Validar nombre
  const nombre = req.body.nombre;
  const usuarioBuscado = await Usuario.findOne({
    nombre
  });
  if (usuarioBuscado) {
    return res.status(400).json({
      mensaje: "Usuario ya existe"
    });
  }
  // Crear Usuario
  const usuario = new Usuario(req.body);
  usuario.password = bcryptjs.hashSync(req.body.password, bcryptjs.genSaltSync());
  usuario.estado = false;
  await usuario.save((err, usuario) => {
    if (!err) {
      res.json(usuario);
    } else {
      console.log('ERROR: ' + err);
      res.status(400).json({msj: err})
  }});
}

const getUsuario = (req = request, res = response) => {
  const _id = req.params._id;
  Usuario.findOne({
    _id
  }, (err, usuario) => res.json(usuario));
}

const getUsuarios = async (req = request, res = response) => {
  res.json(await Usuario.find());
}

const updateUsuario = (req = request, res = response) => {
  const idUsuario = req.params._id;
  Usuario.findByIdAndUpdate(idUsuario, req.body,
    (err, usuario) => {
      if (!err) {
        res.json(usuario);
      } else {
        console.log('ERROR: ' + err);
      }
    });
}

const deleteUsuario = (req = request, res = response) => {
  const idUsuario = req.params._id;
  Usuario.findByIdAndDelete(idUsuario,
    (err, usuario) => {
      if(!err) {
          res.json(usuario);
      } else {
          console.log('ERROR: ' + err);
      }
  })
}


module.exports = {
  createUsuario,
  getUsuario,
  getUsuarios,
  updateUsuario,
  deleteUsuario,
  login
}